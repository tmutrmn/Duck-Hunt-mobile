import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SightingsPage } from '../pages/sightings/sightings';
import { AddSightingPage } from '../pages/add-sighting/add-sighting';
import { AboutPage } from '../pages/about/about';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SpeciesProvider } from '../providers/species/species';
import { SightingsProvider } from '../providers/sightings/sightings';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SightingsPage,
    AddSightingPage,
    AboutPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SightingsPage,
    AddSightingPage,
    AboutPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SpeciesProvider,
    SightingsProvider
  ]
})
export class AppModule {}
