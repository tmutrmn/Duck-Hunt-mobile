import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private bgrImg: string;

  constructor(public navCtrl: NavController) {
    this.bgrImg = "assets/images/duck-hunt.gif";
  }

}
